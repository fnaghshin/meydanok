<footer class="main-footer">
    <div class="container">
        <div class="pull-right hidden-xs">

        </div>
        <strong>Copyright &copy; 2020 <?php echo sitename; ?>.</strong> All rights
        reserved.
    </div>
    <!-- /.container -->
</footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo adminbase; ?>views/assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo adminbase; ?>views/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo adminbase; ?>views/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo adminbase; ?>views/assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo adminbase; ?>views/assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo adminbase; ?>views/assets/dist/js/demo.js"></script>
</body>
</html>
