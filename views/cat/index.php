
    <!-- Full Width Column -->
    <div class="content-wrapper">
        <div class="container-fluid">

            <!-- Main content -->
            <section class="content">
                <?php
                foreach($this->postList as $post)
                {
                ?>
                <div class="col-md-4">
                    <div class="box box-solid">
                        <div class="box-body">
                            <a href="<?php echo url.$post['key']; ?>">
                                <img src="<?php echo base.$post['image']; ?>" style="width: 100%"/>
                            </a>
                            <br />
                            <a href="<?php echo url.$post['key']; ?>">
                                <h3>
                                <?php
                                echo $post['title'];
                                ?>
                                </h3>
                            </a>
                            <br />
                            <?php
                            echo $post['text'];
                            ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <?php
                }
                ?>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
