
    <!-- Full Width Column -->
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Content Header (Page header) -->
            <section class="row">

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="https://picsum.photos/1200/400?random=1" alt="..."
                                style="width: 100%">
                            <div class="carousel-caption">
                                ...
                            </div>
                        </div>
                        <div class="item">
                            <img src="https://picsum.photos/1200/400?random=2" alt="..."
                                style="width: 100%">
                            <div class="carousel-caption">
                                ...
                            </div>
                        </div>
                        <div class="item">
                            <img src="https://picsum.photos/1200/400?random=3" alt="..."
                                 style="width: 100%">
                            <div class="carousel-caption">
                                ...
                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </section>

            <!-- Main content -->
            <section class="content">
                <?php
                foreach($this->postList as $post)
                {
                ?>
                <div class="col-md-4">
                    <div class="box box-solid">
                        <div class="box-body">
                            <a href="<?php echo url.$post['key']; ?>">
                                <img src="<?php echo base.$post['image']; ?>" style="width: 100%"/>
                            </a>
                            <br />
                            <a href="<?php echo url.$post['key']; ?>">
                                <h3>
                                <?php
                                echo $post['title'];
                                ?>
                                </h3>
                            </a>
                            <br />
                            <?php
                            echo $post['text'];
                            ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <?php
                }
                ?>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
