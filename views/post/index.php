
    <!-- Full Width Column -->
    <div class="content-wrapper">
        <div class="container">
            <!-- Content Header (Page header) -->

            <!-- Main content -->
            <section class="content">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-body">
                            <h3>
                                <?php
                                echo $this->post['title'];
                                ?>
                            </h3>
                            <br />
                            <img src="<?php echo base.$this->post['image']; ?>" style="width: 100%;margin-bottom: 10px"/>
                            <br />
                            <?php
                            echo $this->post['text'];
                            ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
