<?php

/*
  # Programmer  : Farzad Naghshin
  # Web Site    : http://farzaad.ir
  # E-Mail      : naghshin.farzad@gmail.com
  # Co Web Site : https://satyarpg.com
  #

  # File        : index
  # Create      : Jul 31, 2018 6:31:26 PM
  # Encoding    : UTF-8
 */

class index extends Controller {

    private $refererUrl;

    function __construct() {
        parent::__construct();
        Session::init();
        $this->LoadModel('core', mPath, 'core');
        $refererUrl = @$_SERVER['HTTP_REFERER'];
        $refererUrl = explode('?', $refererUrl);
        $this->refererUrl = $refererUrl[0];
        $this->view->catList = $this->core->catList();
    }

    //
    //
    // public functions
    //
    //

    public function index()
    {
        $this->view->postList = $this->core->postlist();
        $this->view->render('header');
        $this->view->render('index/index');
        $this->view->render('footer');
    }

    public function cat($id)
    {
        $this->view->postList = $this->core->postlistByCat($id);
        $this->view->render('header');
        $this->view->render('cat/index');
        $this->view->render('footer');
    }

    public function postbykey($key)
    {
        $this->view->post = $this->core->loadPostByKey($key);
        $this->view->render('header');
        $this->view->render('post/index');
        $this->view->render('footer');
    }





    //
    //
    // private functions
//
    //



}
