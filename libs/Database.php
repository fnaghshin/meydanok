<?php

class Database extends PDO {

    function __construct() {
        parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8mb4'"));
        parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function select($sql, $data = array(), $fetchmode = PDO::FETCH_ASSOC) {
        $query = $this->db->prepare($sql);
        foreach ($data as $key => $value) {
            $query->bindValue(":$key", $value);
        }
        $query->execute();
        return $query->fetchAll($fetchmode);
    }

    public function insert($table, $data) {
        $fieldNames = implode('`, `', array_keys($data));
        $fieldNames = '`' . $fieldNames . '`';
        $fieldValues = ':' . implode(", :", array_keys($data));
        $query = $this->prepare("INSERT INTO `$table` ($fieldNames) VALUES ($fieldValues)");

        foreach ($data as $key => $value) {
            $query->bindValue(":$key", $value);
        }
        $query->execute();
        return $this->lastInsertId();
    }

    public function update($table, $data, $where) {
        $fields = NULL;
        foreach ($data as $key => $value) {
            $fields .= '`' . $key . '` = :' . $key . ',';
        }
        $fields = rtrim($fields, ',');
        $query = $this->prepare("UPDATE `$table` SET $fields WHERE $where");

        foreach ($data as $key => $value) {
            $query->bindValue(":$key", $value);
        }
        $query->execute();
    }

    public function delete($table, $data, $limit = 1) {

        foreach ($data as $key => $value) {
            $fields .= ' `' . $key . '` = :' . $key . ' AND';
        }
        $fields = rtrim($fields, 'AND');

        $query = $this->prepare("DELETE FROM $table WHERE $where LIMIT $limit");
        foreach ($data as $key => $value) {
            $query->bindValue(":$key", $value);
        }
        return $query->execute();
    }

}

?>
