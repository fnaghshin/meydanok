<?php

class Cookie{
	
	public static function set($key,$value)
	{
		setcookie($key, base64_encode($value), time() + (864000 * 30), "/");
	}
	
	public static function get($key)
	{
		if(!isset($_COOKIE[$key]))
		{
			return NULL;
		}else{
			return base64_decode($_COOKIE[$key]);
		}
	}
	
}

?>