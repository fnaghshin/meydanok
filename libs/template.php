<?php


class Template{
    
    public static function loadMailTemplate($name,$search,$replace)
    {
        $path = dirname(__FILE__).'/../template/mail/';
        $tem = file_get_contents($path.$name.'.html');
        $tem = str_replace($search, $replace, $tem);
        return $tem;
    }
    
    public static function loadMailImages($name,$prepath)
    {
        $path = dirname(__FILE__).'/../template/mail/';
        $images = file_get_contents($path.$name.'-images');
        $images = explode("\n", $images);
        foreach($images as $image)
        {
            if(strlen($image) > 2)
            {
                $iname = explode('.', $image);
                unset($iname[count($iname)-1]);
                $iname = implode('.', $iname);
                $iname = trim($iname,'.');
                $imgArr[] =array($prepath.$image, $iname , $image);
            }
        }
        return $imgArr;
    }
    
    
}