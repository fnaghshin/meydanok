<?php

$start = '2015-06-08 11:31:23';
$end = '2015-06-08 12:30:33';
print_r(time_diff($start, $end));
  
  
  

function time_diff($start, $end = NULL, $convert_to_timestamp = TRUE) {
  if ($convert_to_timestamp || !is_numeric($start)) {
    $timestamp_start = strtotime($start);
  }
  else {
    $timestamp_start = $start;
  }
  if (!is_null($end) && (!empty($end) && !is_numeric($end))) {
    $timestamp_end = strtotime($end);
  }
  else {
    $timestamp_end = time();
  }
  $start_time = (int) $timestamp_start;
  $end_time = (int) $timestamp_end;

  $start_time_var = 'start_time';
  $end_time_var = 'end_time';
  $pos_neg = 1;

  if ($end_time <= $start_time) {
    $start_time_var = 'end_time';
    $end_time_var = 'start_time';
    $pos_neg = -1;
  }

  $then = new DateTime(date('Y-m-d H:i:s', $$start_time_var));
  $now = new DateTime(date('Y-m-d H:i:s', $$end_time_var));

  $years_then = $then->format('Y');
  $years_now = $now->format('Y');
  $years = $years_now - $years_then;

  $months_then = $then->format('m');
  $months_now = $now->format('m');
  $months = $months_now - $months_then;

  $days_then = $then->format('d');
  $days_now = $now->format('d');
  $days = $days_now - $days_then;

  $hours_then = $then->format('H');
  $hours_now = $now->format('H');
  $hours = $hours_now - $hours_then;

  $minutes_then = $then->format('i');
  $minutes_now = $now->format('i');
  $minutes = $minutes_now - $minutes_then;

  $seconds_then = $then->format('s');
  $seconds_now = $now->format('s');
  $seconds = $seconds_now - $seconds_then;

  if ($seconds < 0) {
    $minutes -= 1;
    $seconds += 60;
  }
  if ($minutes < 0) {
    $hours -= 1;
    $minutes += 60;
  }
  if ($hours < 0) {
    $days -= 1;
    $hours += 24;
  }
  $months_last = $months_now - 1;
  if ($months_now == 1) {
    $years_now -= 1;
    $months_last = 12;
  }

  if ($months_last == 9 || $months_last == 4 || $months_last == 6 || $months_last == 11) {
    $days_last_month = 30;
  }
  else if ($months_last == 2) {
    if (($years_now % 4) == 0) {
      $days_last_month = 29;
    }
    else {
      $days_last_month = 28;
    }
  }
  else {
    $days_last_month = 31;
  }
  if ($days < 0) {
    $months -= 1;
    $days += $days_last_month;
  }
  if ($months < 0) {
    $years -= 1;
    $months += 12;
  }

  $out = new stdClass;
  $out->years = (int) $years * $pos_neg;
  $out->months = (int) $months * $pos_neg;
  $out->days = (int) $days * $pos_neg;
  $out->hours = (int) $hours * $pos_neg;
  $out->minutes = (int) $minutes * $pos_neg;
  $out->seconds = (int) $seconds * $pos_neg;
 
    $seconds = strtotime($end) - strtotime($start);
	$days = floor($seconds / 86400);
	$hours = floor($seconds  / 3600);
	$minutes = floor($seconds/60);
	
 	$time['years'] = $out->years;
	$time['month'] = $out->months + $out->years * 12;
	$time['days'] = $days;
	$time['hours'] = $hours;
	$time['minutes'] = $minutes;
	$time['seconds'] = $seconds;
	return($time);
}  
?>