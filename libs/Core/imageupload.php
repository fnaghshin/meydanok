<?php

##########################################################
##
##	file handler class
##  farzad naghshin
##  naghshin.farzad@gmail.com
##  http://www.fzcoders.com
##
##########################################################

class ImageUpload
{
	
	public $MaxSize = 100000000;
	public $Path;
	public $ImageFile;
	public $NewWidth;
	public $NewHeight;
	public $ResizePercent;
	public $Rename = true;
	private $uploadedfile;
	private $src;
	private $Width;
	private $Height;
	private $extention;
	public $NewName = "";
	private $watermark = "";
	public $wm = true;
	private $watermarkop = 0;
	private $FileType;
	
	function __construct()
	{
		$this->watermark = dirname(__file__)."/watermark.jpg";
		$this->wm = true;
	}
	
	private function getExtension($str) {

         $i = strrpos($str,".");
         if (!$i) { return ""; } 
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
		 
	}
	
	public function GetFile()
	{
		
		$image = $this->ImageFile["name"];
		
		$uploadedfile = $this->ImageFile['tmp_name'];
		
		if ($image){
					
			$filename = stripslashes($this->ImageFile['name']);
			
			$extension = $this->getExtension($filename);
			
		 	$extension = strtolower($extension);
			
			$this->extention = $extension;
			
		if (($extension != "jpg") && ($extension != "jpeg") 
		&& ($extension != "png") && ($extension != "gif")) 
		{
			return 'Unknown Image';
		}
		else
		{
			$size=filesize($this->ImageFile['tmp_name']);
			 
			if ($size > $this->MaxSize*1024)
			{
				echo "exceeded the size limit";
			}
			 
			if($extension=="jpg" || $extension=="jpeg" )
			{
				$this->uploadedfile = $this->ImageFile['tmp_name'];
				$this->src = imagecreatefromjpeg($this->uploadedfile);
				$this->FileType = "j";
			}
			else if($extension=="png")
			{
				$this->uploadedfile = $this->ImageFile['tmp_name'];
				$this->src = imagecreatefrompng($this->uploadedfile);
				$this->FileType = "p";
			}
			else 
			{
				$this->uploadedfile = $this->ImageFile['tmp_name'];
				$this->src = imagecreatefromgif($this->uploadedfile);
				$this->FileType = "g";
			}
		}
		
		}
	}
	
	private function RenameFile($Name)
	{
		
		if($this->NewName == "")
		{
		
			return md5($Name . time() . date('Y-m-d')) . "." . $this->extention;
		
		}else{
			
			return $this->NewName .'.'. $this->extention;
			
		}
		
	}
	
	private function ImageSize()
	{
		
		list($width,$height) = getimagesize($this->uploadedfile);
		
		$this->Width = $width;
		
		$this->Height = $height;
		
	}
	
	public function Upload()
	{
		define('UPLOADED_IMAGE_DESTINATION', $this->Path);
		define('PROCESSED_IMAGE_DESTINATION', $this->Path.'2/');
		
		$temp_file_path = $this->ImageFile['tmp_name'];
		$temp_file_name = $this->ImageFile['name'];
		list(, , $temp_type) = getimagesize($temp_file_path);
		if ($temp_type === NULL) {
			return false;
		}
			switch ($temp_type) {
				case IMAGETYPE_GIF:
					$bname = "gif";
					break;
				case IMAGETYPE_JPEG:
					$bname = "jpg";
					break;
				case IMAGETYPE_PNG:
					$bname = "png";
					break;
				default:
					return false;
		}
		if($this->Rename == true)
		{
			
			$filename = $this->Path . $this->RenameFile($this->ImageFile['name']);
			
		}else{
			
			$filename = $this->Path . $this->ImageFile['name'];
			
		}
		$uploaded_file_path = $filename .$bname;
		$processed_file_path = $filename.$bname;
		move_uploaded_file($temp_file_path, $uploaded_file_path);
		if($this->wm == true)
		{
			$result = $this->addwatermark($uploaded_file_path, $processed_file_path);
			if ($result === false) {
				return false;
			} else {
				return $processed_file_path;
			}
		}
			return $processed_file_path;	
	}
	
	public function addwatermark($source_file_path, $output_file_path)
	{
		
		define('WATERMARK_OVERLAY_IMAGE', dirname(__file__).'/'.$this->watermark);
		define('WATERMARK_OVERLAY_OPACITY', 30);
		define('WATERMARK_OUTPUT_QUALITY', 90);
		list($source_width, $source_height, $source_type) = getimagesize($source_file_path);
		if ($source_type === NULL) {
			return false;
		}
			switch ($source_type) {
				case IMAGETYPE_GIF:
					$source_gd_image = imagecreatefromgif($source_file_path);
					break;
				case IMAGETYPE_JPEG:
					$source_gd_image = imagecreatefromjpeg($source_file_path);
					break;
				case IMAGETYPE_PNG:
					$source_gd_image = imagecreatefrompng($source_file_path);
					break;
				default:
					return false;
		}
		$overlay_gd_image = imagecreatefrompng(WATERMARK_OVERLAY_IMAGE);
		
		$background = imagecolorallocate($overlay_gd_image, 0, 0, 0);
		imagecolortransparent($overlay_gd_image, $background);
		imagealphablending($overlay_gd_image,false);
		imagesavealpha($overlay_gd_image,true);
		$newImg = imagecreatetruecolor($source_width, $source_height);
		imagealphablending($newImg, false);
		imagesavealpha($newImg,true);
		$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
		imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
		$source_height_n = ceil((49/177) * $source_width);
		imagecopyresampled($newImg, $overlay_gd_image, 0, 0, 0, 0, $source_width, $source_height_n,400,143);
		imagepng($overlay_gd_image,$newImg);
		$overlay_gd_image = $newImg;
		$overlay_width = imagesx($overlay_gd_image);
		$overlay_height = imagesy($overlay_gd_image);
		$overlay_height = $source_height_n;
		imagecopy(
		$source_gd_image,
		$overlay_gd_image,
		(($source_width/2) - ($overlay_width/2)),
		(($source_height/2) - ($overlay_height/2)),
		0,
		0,
		$overlay_width,
		$overlay_height
		);
		imagejpeg($source_gd_image, $output_file_path, WATERMARK_OUTPUT_QUALITY);
		imagedestroy($source_gd_image);
		imagedestroy($overlay_gd_image);
		
	}
	
	public function UploadAndFullResize()
	{
		
		$this->GetFile();
		
		$this->ImageSize();

		$tmp=imagecreatetruecolor($this->NewWidth,$this->NewHeight);

		imagealphablending($tmp, false);
		
		imagesavealpha($tmp,true);
		
		$transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
		
		imagefilledrectangle($tmp, 0, 0, $this->NewWidth, $this->NewHeight, $transparent);
				
		imagecopyresampled($tmp,$this->src,0,0,0,0,$this->NewWidth,$this->NewHeight,$this->Width,$this->Height);
		
		if($this->Rename == true)
		{
			
			$filename = $this->Path . $this->RenameFile($this->ImageFile['name']);
			
		}else{
			
			$filename = $this->Path . $this->ImageFile['name'];
			
		}
		
		switch($this->FileType)
		{
			
			case "j": imagejpeg($tmp,$filename,100); break;
			
			case "p": imagepng($tmp,$filename) ; break;
			
			case "g": imagegif($tmp,$filename); break;
			
		}
		$uploaded_file_path = $filename;
		$processed_file_path = $filename;
		imagedestroy($tmp);
		if($this->wm == true)
		{
			$result = $this->addwatermark($uploaded_file_path, $processed_file_path);
			if ($result === false) {
				return false;
			} else {
				return $processed_file_path;
			}
		}
			return $processed_file_path;	
		
	}
	
	public function UploadAndResizeByWidth()
	{
		
		$this->GetFile();
		
		$this->ImageSize();
		
		$NewHeight =($this->Height/$this->Width) * $this->NewWidth;
		
		$tmp=imagecreatetruecolor($this->NewWidth,$NewHeight);
		
		imagecopyresampled($tmp,$this->src,0,0,0,0,$this->NewWidth,$NewHeight,$this->Width,$this->Height);
		
		if($this->Rename == true)
		{
			
			$filename = $this->Path . $this->RenameFile($this->ImageFile['name']);
			
		}else{
			
			$filename = $this->Path . $this->ImageFile['name'];
			
		}
		
		imagejpeg($tmp,$filename,100);
		$uploaded_file_path = $filename;
		$processed_file_path = $filename;
		imagedestroy($tmp);
		if($this->wm == true)
		{
			$result = $this->addwatermark($uploaded_file_path, $processed_file_path);
			if ($result === false) {
				return false;
			} else {
				return $processed_file_path;
			}
		}
			return $processed_file_path;	
		
		
		
	}
	
	
	public function UploadAndResizeByHeight()
	{
		
		$this->GetFile();
		
		$this->ImageSize();
		
		$NewWidth =($this->Width/$this->Height) * $this->NewHeight;
		
		$tmp=imagecreatetruecolor($NewWidth,$this->NewHeight);
		
		imagealphablending($tmp, false);
		
		imagesavealpha($tmp,true);
				
		imagecopyresampled($tmp,$this->src,0,0,0,0,$NewWidth,$this->NewHeight,$this->Width,$this->Height);
		
		if($this->Rename == true)
		{
			
			$filename = $this->Path . $this->RenameFile($this->ImageFile['name']);
			
		}else{
			
			$filename = $this->Path . $this->ImageFile['name'];
			
		}
		
		switch($this->FileType)
		{
			
			
			case "p": imagepng($tmp,$filename) ; break;
			
			case "g": imagegif($tmp,$filename); break;
			
		}
		
		imagedestroy($tmp);
		$uploaded_file_path = $filename;
		$processed_file_path = $filename;
		if($this->wm == true)
		{
			$result = $this->addwatermark($uploaded_file_path, $processed_file_path);
			if ($result === false) {
				return false;
			} else {
				return $processed_file_path;
			}
		}
			return $processed_file_path;	
		
	}
	
	public function DestroyImage()
	{
		
		imagedestroy($this->src);
		
	}	
	
}



?>