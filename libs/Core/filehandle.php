<?php

##########################################################
##
##	file handler class
##	farzad naghshin
##	naghshin.farzad@gmail.com
##	http://www.fzcoders.com
##
##########################################################

class filehandel
{
	
	
	##############################
	##
	##	Public Functions
	##
	##############################
	
	
	/*
	type = {w : write , rw : read and write}
	*/
	public function writetofile($data,$file,$type='rw',$divider='^^')
	{
		if($this->checkfile($file) == false)
			$this->createfile($file);
		$olddata = '';
		if($type == 'rw')
			$olddata = $this->getfiledata($file);
		if($olddata != '')
			$data = $olddata.$divider.$data;
		file_put_contents($file,$data);
	}
	
	public function getfilelist($folder)
	{
		$filelist = scandir($folder);
		unset($filelist[0]);
		unset($filelist[1]);
		return $filelist;
	}
	
	public function getfile($file,$divider='^^')
	{
		$data = $this->getfiledata($file);
		$data = explode($divider,$data);
		return $data;
	}
	
	
	##############################
	##
	##	Private Functions
	##
	##############################
	
	private function checkfile($file)
	{
		if(file_exists($file))
		{
			return true;
		}else{
			return false;
		}
	}
	
	private function createfile($file)
	{
		fopen($file,'w');
	}
	
	private function getfiledata($file)
	{
		return file_get_contents($file);
	}
	
}

$filehandle = new filehandel();
echo '<pre>';
print_r($filehandle->getfile('../../tmp/txt.txt','<br>'));
?>