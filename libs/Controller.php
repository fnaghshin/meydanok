<?php

class Controller {

    function __construct() {
        $this->view = new View();
    }

    function LoadModel($name, $path , $call='model') {
        $file = $path . $name . '_model.php';
        if (file_exists($file)) {
            require_once $file;
            $modelName = $name . '_model';
            $this->$call = new $modelName;
        }
    }

}

?>
