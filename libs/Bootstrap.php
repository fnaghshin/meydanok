<?php

class Bootstrap {

    private $_url = NULL;
    private $_controller = NULL;
    private $_controllerPath = 'controllers/';
    private $_modelPath = 'models/';
    private $_errorFile = 'error.php';
    public $Lang;

    function __construct() {

    }

    public function Start() {
        // Set Pathes Define
        define('cPath', $this->_controllerPath);
        define('mPath', $this->_modelPath);
        // Get Url
        $this->_getURL();
        // Set Default Controller
        $this->_loadDefaultController();
        // Load Controller
        $this->_loadController();
        // Call Method
        $this->_calMethod();
    }

    public function setControllerPath($path) {
        $this->_controllerPath = trim($path, '/') . '/';
    }

    public function setModelPath($path) {
        $this->_modelPath = trim($path, '/') . '/';
    }

    public function setErrorFile($path) {
        $this->_errorFile = $path;
    }

    private function _getURL() {
        $this->_url = isset($_GET['url']) ? $_GET['url'] : null;
		$this->_url = urldecode($this->_url);
        $this->_url = rtrim($this->_url, '/');
        //$this->_url = filter_var($this->_url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $this->_url);
    }

    private function _loadDefaultController() {
        if (empty($this->_url[0])) {
            $this->_url[0] = 'index';
        }
        if (empty($this->_url[1])) {
            $this->_url[1] = 'index';
        }
    }

    private function _loadController() {
        $file = $this->_controllerPath . $this->_url[0] . '.php';
        if (file_exists($file)) {
            require_once $file;
            $this->_controller = new $this->_url[0];
            $this->_controller->LoadModel($this->_url[0], $this->_modelPath);
        } else {
            $key = $this->_url[0];
            $db = new Database();
            $query = $db->prepare("SELECT * FROM `post` WHERE `key`=:key");
            $query->execute(array(':key'=>$key));
            if($query->rowCount() > 0)
            {
                $this->_url[0] = 'index';
                $file = $this->_controllerPath . $this->_url[0] . '.php';
                require_once $file;
                $this->_controller = new $this->_url[0];
                $this->_controller->LoadModel($this->_url[0], $this->_modelPath);
                $this->_url[1] = 'postbykey';
                $this->_url[2] = $key;
            }else {
                $this->_url[2] = $this->_url[1];
                $this->_url[1] = $this->_url[0];
                $this->_url[0] = 'index';
                $file = $this->_controllerPath . $this->_url[0] . '.php';
                require_once $file;
                $this->_controller = new $this->_url[0];
                $this->_controller->LoadModel($this->_url[0], $this->_modelPath);
            }
        }
    }

    private function _calMethod() {

        $lenght = count($this->_url);
        if ($lenght > 1) {
            if (!method_exists($this->_controller, $this->_url[1])) {
                $this->_error();
                return false;
            }
        }

        switch ($lenght) {
            case 5:
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4]);
                break;
            case 4:
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3]);
                break;
            case 3:
                $this->_controller->{$this->_url[1]}($this->_url[2]);
                break;
            default:
                $this->_controller->{$this->_url[1]}();
                break;
        }
    }

    function loadsetting() {
        /*$db = new Database();
        $query = $db->prepare("SELECT * FROM `setting`");
        $query->execute();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            define($row['setting_key'], $row['setting_value']);
        }
        $db = null;
        unset($db);*/
    }

    function _error() {
        require_once $this->_controllerPath . $this->_errorFile;
        $this->_controller = new error();
        $this->_controller->index();
        exit;
    }

}

?>
