
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category List
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo adminurl; ?>">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">
                Category List
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
        <table class="table table-hover table-striped table-bordered">
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Action</th>
            </tr>
            <?php
            foreach($this->catlist as $cat)
            {
            ?>
            <tr>
                <td><?php echo $cat['id']; ?></td>
                <td><?php echo $cat['title']; ?></td>
                <td>
                    <a href="<?php echo adminurl.'cat/edit/'.$cat['id']; ?>"
                        class="btn btn-primary">
                        Edit
                    </a>
                     |
                    <a href="<?php echo adminurl.'cat/del/'.$cat['id']; ?>"
                       onclick="return confirm('Are you going to delete the item?')"
                       class="btn btn-danger">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
            <?php
            }
            ?>
        </table>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->




