
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Category
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo adminurl; ?>">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="<?php echo adminurl; ?>cat/">
                    <i class="fa fa-list"></i>
                    Category List
                </a>
            </li>
            <li class="active">
                Edit Category
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <form action="<?php echo adminurl.'cat/runedit/'.$this->catInfo['id']; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title">Title : </label>
                <input type="text" name="title" id="title" class="form-control"
                    value="<?php echo $this->catInfo['title'] ; ?>"/>
            </div>
            <div class="form-group" style="text-align: center">
                <button class="btn btn-primary" type="submit">
                    Update Category Info
                </button>
            </div>
        </form>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->




