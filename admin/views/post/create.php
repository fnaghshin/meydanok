
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create Post
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo adminurl; ?>">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="<?php echo adminurl; ?>post/">
                    <i class="fa fa-list"></i>
                    Post List
                </a>
            </li>
            <li class="active">
                Create Post
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <form action="<?php echo adminurl.'post/runcreate/'; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title">Title : </label>
                <input type="text" name="title" id="title" class="form-control"
                    value="" onblur="genrateKey()"/>
            </div>
            <div class="form-group">
                <label for="key">URL : </label>
                <input type="text" name="key" id="key" class="form-control"
                       value="" readonly/>
            </div>
            <div class="form-group">
                <label for="title">Category : </label>
                <select name="cat" id="cat" class="form-control">
                    <?php
                    foreach($this->catList as $catItem)
                    {
                        echo '<option value="'.$catItem['id'].'">'.$catItem['title'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="image">Image : </label>
                <input type="file" name="image" id="image" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="text">Text : </label>
                <textarea name="text" id="text" class="form-control"
                    ></textarea>
            </div>
            <div class="form-group" style="text-align: center">
                <button class="btn btn-primary" type="submit">
                    Create Post
                </button>
            </div>
        </form>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->




<?php
global $js;
$js = '<!-- CK Editor -->
<script src="'.adminbase.'views/assets/bower_components/ckeditor/ckeditor.js"></script>
<script>
    $(function () {
        CKEDITOR.replace("text",{
            "filebrowserImageUploadUrl": "'.adminbase.'views/assets/bower_components/ckeditor/plugins/imgupload.php"
        });
        CKEDITOR.config.extraAllowedContent = "audio[*]{*}";  //to allow audio tag

        CKEDITOR.filter.allowedContentRules;
    })
    function genrateKey()
    {
        var title = $("#title").val();
        $.post("'.adminurl.'post/getPostKey",{
            "title":title
        },function(data){
            $("#key").val("'.url.'"+data);            
        });
    }
</script>
';
?>

