
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Post
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo adminurl; ?>">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="<?php echo adminurl; ?>post/">
                    <i class="fa fa-list"></i>
                    Post List
                </a>
            </li>
            <li class="active">
                Edit Post
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <form action="<?php echo adminurl.'post/runedit/'.$this->postInfo['id']; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title">Title : </label>
                <input type="text" name="title" id="title" class="form-control"
                       value="<?php echo $this->postInfo['title']; ?>" onblur="genrateKey()"/>
            </div>
            <div class="form-group">
                <label for="key">URL : </label>
                <input type="text" name="key" id="key" class="form-control"
                       value="<?php echo url.$this->postInfo['key']; ?>" readonly/>
            </div>
            <div class="form-group">
                <label for="title">Category : </label>
                <select name="cat" id="cat" class="form-control">
                    <?php
                    foreach($this->catList as $catItem)
                    {
                        $selected = ($this->postInfo['cat'] == $catItem['id'])?'selected=""':'';
                        echo '<option value="'.$catItem['id'].'" '.$selected.'>'.$catItem['title'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="image">Image : </label>
                <div class="row">
                    <div class="col-sm-9">
                        <input type="file" name="image" id="image" class="form-control"/>
                    </div>
                    <div class="col-sm-3">
                        <img src="<?php echo base.$this->postInfo['image']; ?>" style="width: 100%"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="text">Text : </label>
                <textarea name="text" id="text" class="form-control"
                ><?php echo $this->postInfo['text']; ?></textarea>
            </div>
            <div class="form-group" style="text-align: center">
                <button class="btn btn-primary" type="submit">
                    Update Post Info
                </button>
            </div>
        </form>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->




<?php
global $js;
$js = '<!-- CK Editor -->
<script src="'.adminbase.'views/assets/bower_components/ckeditor/ckeditor.js"></script>
<script>
    $(function () {
        CKEDITOR.replace("text",{
            "filebrowserImageUploadUrl": "'.adminbase.'views/assets/bower_components/ckeditor/plugins/imgupload.php"
        });
        CKEDITOR.config.extraAllowedContent = "audio[*]{*}";  //to allow audio tag

        CKEDITOR.filter.allowedContentRules;
    })
    function genrateKey()
    {
        var title = $("#title").val();
        $.post("'.adminurl.'post/getPostKey",{
            "title":title
        },function(data){
            $("#key").val("'.url.'"+data);            
        });
    }
</script>
';
?>

