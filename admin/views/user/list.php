
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User List
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo adminurl; ?>">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">
                User List
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
        <table class="table table-hover">
            <tr>
                <th>ID</th>
                <th>Full Name</th>
                <th>Username</th>
                <th>E-Mail</th>
                <th>Created</th>
                <th>Access</th>
                <th>Action</th>
            </tr>
            <?php
            foreach($this->userlist as $user)
            {
            ?>
            <tr>
                <td><?php echo $user['id']; ?></td>
                <td><?php echo $user['fullname']; ?></td>
                <td><?php echo $user['username']; ?></td>
                <td><?php echo $user['mail']; ?></td>
                <td><?php echo $user['create_at']; ?></td>
                <td>
                    <?php
                    echo ($user['type'] == 2)?'<span class="label label-success">Admin</span>':'<span class="label label-info">Normal User</span>';
                    ?>
                </td>
                <td>
                    <a href="<?php echo adminurl.'user/edit/'.$user['id']; ?>"
                        class="btn btn-primary">
                        Edit
                    </a>
                </td>
            </tr>
            <?php
            }
            ?>
        </table>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->




