
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit User
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo adminurl; ?>">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="<?php echo adminurl; ?>user/">
                    <i class="fa fa-list"></i>
                    User List
                </a>
            </li>
            <li class="active">
                Edit User
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <form action="<?php echo adminurl.'user/runedit/'.$this->userInfo['id']; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="fullname">Fullname : </label>
                <input type="text" name="fullname" id="fullname" class="form-control"
                    value="<?php echo $this->userInfo['fullname'] ; ?>"/>
            </div>
            <div class="form-group">
                <label for="username">Username : </label>
                <input type="text" name="username" id="username" class="form-control"
                       value="<?php echo $this->userInfo['username'] ; ?>"/>
            </div>
            <div class="form-group">
                <label for="password">New Password : </label>
                <input type="text" name="password" id="password" class="form-control"
                       placeholder="Değiştirmek istemiyorsanız boş bırakın"/>
            </div>
            <div class="form-group">
                <label for="mail">E-Mail : </label>
                <input type="text" name="mail" id="mail" class="form-control"
                       value="<?php echo $this->userInfo['mail'] ; ?>"/>
            </div>
            <div class="form-group">
                <label for="avatar">Avatar : </label>
                <div class="row">
                    <div class="col-sm-9">
                        <input type="file" name="avatar" id="avatar" class="form-control"/>
                    </div>
                    <div class="col-sm-3">
                        <img src="<?php echo base.$this->userInfo['avatar'] ; ?>" style="max-width: 100%"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="bio">Bio : </label>
                <textarea name="bio" id="bio" class="form-control"
                    rows="4"
                    ><?php echo $this->userInfo['bio'] ; ?></textarea>
            </div>
            <div class="form-group">
                <label for="type">Group : </label>
                <select name="type" id="type" class="form-control">
                    <option value="1" <?php echo ($this->userInfo['type'] == 1)?'selected=""':'' ; ?>>Normal User</option>
                    <option value="2" <?php echo ($this->userInfo['type'] == 2)?'selected=""':'' ; ?>>Admin Access</option>
                </select>
            </div>
            <div class="form-group" style="text-align: center">
                <button class="btn btn-primary" type="submit">
                    Update User Info
                </button>
            </div>
        </form>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->




