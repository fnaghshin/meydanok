<?php


class user_model extends Model {

    private $_userid;
    private $_totalPerPage;
    public function __construct() {
        parent::__construct();
        $this->_userid = Session::get('userid');
        $this->_totalPerPage = 30;
        if(defined('totalperpage') && totalperpage > 0)
            $this->_totalPerPage = totalperpage;
    }

    //
    //
    // public functions
    //
    //


    public function getList()
    {
        $query = $this->db->prepare("SELECT `id`,`username`,`mail`,`fullname`,`create_at`,`type` FROM `user` ORDER BY `create_at` DESC");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function loadItem($id)
    {
        $query = $this->db->prepare("SELECT * FROM `user` WHERE `id`=:id");
        $query->execute(
                    array(
                        ':id'=>$id
                    )
                );
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function login($mail,$pass)
    {
        $pass = sha1($pass);
        $query = $this->db->prepare("SELECT * FROM `user` 
            WHERE 
            (
                `mail`=:mail
                OR 
                `username`=:username
            )
            AND 
            `password`=:pass");
        $query->execute(
                    array(
                        ':mail'=>$mail,
                        ':username'=>$mail,
                        ':pass'=>$pass
                    )
                );
        if($query->rowCount() == 0)
            return Core::SetArray (true, 'msg100001', '');
        $userInfo = $query->fetch(PDO::FETCH_ASSOC);
        if($userInfo['type'] == 0)
            return Core::SetArray (true, 'msg100002', '');
        $this->setSessions($userInfo);
        return Core::SetArray(false, 'msg100003', '');
    }

    public function create($fullname,$username,$password,$mail,$avatar,$bio,$type)
    {
        if(!filter_var($mail, FILTER_VALIDATE_EMAIL))
            return Core::SetArray (true, 'msg100004'.$mail, '');
        if(strlen($password) < 6)
            return Core::SetArray (true, 'msg100005', '');
        if($fullname == '')
            return Core::SetArray (true, 'msg200001', '');
        $query = $this->db->prepare("SELECT `id` FROM `user` WHERE `mail`=:mail");
        $query->execute(
                    array(
                        ':mail'=>$mail
                    )
                );
        if($query->rowCount() > 0)
            return Core::SetArray (true, 'msg100006', '');
        $query = $this->db->prepare("SELECT `id` FROM `user` WHERE `username`=:username");
        $query->execute(
            array(
                ':username'=>$username
            )
        );
        if($query->rowCount() > 0)
            return Core::SetArray (true, 'msg100006', '');
        $password = sha1($password);
        if(strlen($avatar['name'])>1)
        {
            $avatar = $this->upload($avatar);
        }else{
            $avatar = 'noavatar.jpg';
        }
        $query = $this->db->prepare("INSERT INTO `user` SET 
            `username`=:username,
            `password`=:password,
            `mail`=:mail,
            `avatar`=:avatar,
            `fullname`=:fullname,
            `bio`=:bio,
            `type`=:type,
            `create_at`=now()");
        $query->execute(
                    array(
                        ':username'=>$username,
                        ':password'=>$password,
                        ':mail'=>$mail,
                        ':avatar'=>$avatar,
                        ':fullname'=>$fullname,
                        ':bio'=>$bio,
                        ':type'=>$type
                    )
                );
        $userid = $this->db->lastInsertId();
        return Core::SetArray(false, 'msg100007', array('id'=>$userid));
    }

    public function edit($userid,$fullname,$username,$password,$mail,$avatar,$bio,$type)
    {
        if(!filter_var($mail, FILTER_VALIDATE_EMAIL))
            return Core::SetArray (true, 'msg100004', '');
        if($fullname == '')
            return Core::SetArray (true, 'msg200001', '');
        $query = $this->db->prepare("SELECT * FROM `user` 
            WHERE 
            `mail`=:mail 
            AND 
            `id`!=:userid");
        $query->execute(
                    array(
                        ':mail'=>$mail,
                        ':userid'=>$userid
                    )
                );
        if($query->rowCount() > 0)
            return Core::SetArray (true, 'msg100006', '');
        if(strlen($password) > 0)
        {
            $password = Core::MyHash($password);
            $query = $this->db->prepare("UPDATE `user` SET 
                `password`=:pass 
                WHERE 
                `id`=:userid");
            $query->execute(
                        array(
                            ':pass'=>$password,
                            ':userid'=>$userid
                        )
                    );
        }
        if(strlen($avatar['name'])>1)
        {
            $avatar = $this->upload($avatar);
            $query = $this->db->prepare("UPDATE `user` SET 
                `avatar`=:avatar 
                WHERE 
                `id`=:userid");
            $query->execute(
                        array(
                            ':avatar'=>$avatar,
                            ':userid'=>$userid
                        )
                    );
        }else{
            $avatar = 'noavatar.jpg';
        }
        $query = $this->db->prepare("UPDATE `user` SET 
            `username`=:username,
            `mail`=:mail,
            `fullname`=:fullname,
            `bio`=:bio,
            `type`=:type
            WHERE 
            `id`=:userid");
        $query->execute(
                    array(
                        ':username'=>$username,
                        ':mail'=>$mail,
                        ':fullname'=>$fullname,
                        ':bio'=>$bio,
                        ':type'=>$type,
                        ':userid'=>$userid
                    )
                );
        if($query->rowCount() > 0)
            return Core::SetArray(false, 'msg100008', '');
        return Core::SetArray(true, 'msg200002', '');
    }


    //
    //
    // private functions
    //
    //

    private function setSessions($userInfo)
    {
        Session::init();
        Session::set('userid', $userInfo['id']);
        Session::set('fullname', $userInfo['fullname']);
        Session::set('avatar', $userInfo['user_avatar']);
        Session::set('type', $userInfo['type']);
        Session::set('islogin',1);
        if($userInfo['type'] >=2 )
        {
            Session::set('isadmin',1);
        }else{
            Session::set('isadmin',0);
        }
    }

    public function upload($avatar)
    {
        $path = dirname(__FILE__).'/../../assets/img/';
        $avatar = Core::UploadImage($avatar, $path, false, false, false,false);
        return str_replace(dirname(__FILE__).'/../..','',$avatar);
    }

}
