<?php


class cat_model extends Model {

    private $_userid;

    public function __construct() {
        parent::__construct();
        $this->_userid = Session::get('userid');
    }

    //
    //
    // public functions
    //
    //

    public function getList()
    {
        $query = $this->db->prepare("SELECT * FROM `category`");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function loadItem($id)
    {
        $query = $this->db->prepare("SELECT * FROM `category` WHERE `id`=:id");
        $query->execute(
            array(
                ':id'=>$id
            )
        );
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function add($name)
    {
        if($name == '')
            return Core::SetArray (true, 'msg200001', '');
        $query = $this->db->prepare("INSERT INTO `category` SET 
            `title`=:name");
        $query->execute(
            array(
                ':name'=>$name
            )
        );
        $id = $this->db->lastInsertId();
        if($query->rowCount() > 0)
            return Core::SetArray (false, 'msg100101',  array('id'=>$id));
        return Core::SetArray(true, 'msg200000', '');
    }

    public function edit($id,$name)
    {
        if($name == '')
            return Core::SetArray (true, 'msg200001', '');
        $query = $this->db->prepare("UPDATE `category` SET 
            `title`=:name 
            WHERE 
            `id`=:id");
        $query->execute(
            array(
                ':name'=>$name,
                ':id'=>$id
            )
        );
        if($query->rowCount() > 0)
            return Core::SetArray (false, 'msg100102', '');
        return Core::SetArray(true, 'msg200002', '');
    }

    public function del($id)
    {
        $query = $this->db->prepare("DELETE FROM `category` WHERE `id`=:id");
        $query->execute(
            array(
                ':id'=>$id
            )
        );
        return Core::SetArray(false, 'msg100103', '');
    }







    //
    //
    // private functions
    //
    //
}
