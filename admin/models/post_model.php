<?php


class post_model extends Model {

    private $_userid;

    public function __construct() {
        parent::__construct();
        $this->_userid = Session::get('userid');
    }

    //
    //
    // public functions
    //
    //

    public function getList()
    {
        $query = $this->db->prepare("SELECT * FROM `post`");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function loadItem($id)
    {
        $query = $this->db->prepare("SELECT * FROM `post` WHERE `id`=:id");
        $query->execute(
            array(
                ':id'=>$id
            )
        );
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function create($title,$key,$cat,$text,$image)
    {
        if($title == '' || $key == '' || $cat == '' || $text == '')
            return Core::SetArray(true,'msg','');
        $key = str_replace(url,'',$key);
        if($image['name'] != '')
        {
            $image = $this->upload($image);
        }else{
            $image = '/assets/img/noimage.jpg';
        }
        $query = $this->db->prepare("INSERT INTO `post` SET 
                `key`=:key,
                `title`=:title,
                `image`=:image,
                `text`=:text,
                `cat`=:cat,
                `owner`=:userid,
                `status`=0,
                `created_at`=now(),
                `updated_at`=now()");
        $query->execute(array(
            ':key'=>$key,
            ':title'=>$title,
            ':image'=>$image,
            ':text'=>$text,
            ':cat'=>$cat,
            ':userid'=>$this->_userid
        ));
        $postid = $this->db->lastInsertId();
        if($postid > 0)
            return Core::SetArray(false,'Post Created Success',array('id'=>$postid));
        return Core::SetArray(true,'DB Error','');
    }

    public function edit($id,$title,$key,$cat,$text,$image)
    {
        if($title == '' || $key == '' || $cat == '' || $text == '')
            return Core::SetArray(true,'msg','');
        $postInfo = $this->loadItem($id);
        $key = str_replace(url,'',$key);
        if($image['name'] != '')
        {
            @unlink(dirname(__FILE__).'/../..'.$postInfo['image']);
            $image = $this->upload($image);
        }else{
            $image = $postInfo['image'];
        }
        $query = $this->db->prepare("UPDATE `post` SET 
                `key`=:key,
                `title`=:title,
                `image`=:image,
                `text`=:text,
                `cat`=:cat,
                `updated_at`=now()");
        $query->execute(array(
            ':key'=>$key,
            ':title'=>$title,
            ':image'=>$image,
            ':text'=>$text,
            ':cat'=>$cat
        ));
        if($query->rowCount() > 0)
            return Core::SetArray(false,'Post Edited Success','');
        return Core::SetArray(true,'DB Error','');
    }

    public function del($id)
    {
        $postInfo = $this->loadItem($id);
        @unlink(dirname(__FILE__).'/../..'.$postInfo['image']);
        $query = $this->db->prepare("DELETE FROM `post` WHERE `id`=:id");
        $query->execute(
            array(
                ':id'=>$id
            )
        );
        return Core::SetArray(false, 'msg100103', '');
    }

    public function getKey($title)
    {
        $title = str_replace('  ',' ',$title);
        $key = str_replace(' ','-',$title);
        $isKey = false;
        $i = 1;
        while (!$isKey) {
            $query = $this->db->prepare("SELECT * FROM `post` WHERE `key`=:key");
            $query->execute(array(
                ':key' => $key
            ));
            if ($query->rowCount() == 0) {
                $isKey = true;
                break;
            }else{
                $key = $key.$i;
                $i++;
            }
        }
        return $key;
    }

    //
    //
    // private functions
    //
    //

    public function upload($avatar)
    {
        $path = dirname(__FILE__).'/../../assets/img/';
        $avatar = Core::UploadImage($avatar, $path, false, false, false,false);
        return str_replace(dirname(__FILE__).'/../..','',$avatar);
    }
}
