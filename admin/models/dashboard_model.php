<?php


class dashboard_model extends Model {

    private $_userid;

    public function __construct() {
        parent::__construct();
        $this->_userid = Session::get('userid');
    }

    //
    //
    // public functions
    //
    //

    public function totalUser()
    {
        $query = $this->db->prepare("SELECT COUNT(*) AS `total` FROM `user`");
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result['total'];
    }

    public function totalCat()
    {
        $query = $this->db->prepare("SELECT COUNT(*) AS `total` FROM `category`");
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result['total'];
    }

    public function totalPost()
    {
        $query = $this->db->prepare("SELECT COUNT(*) AS `total` FROM `post`");
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result['total'];
    }

}
