<?php

/*
  # Programmer  : Farzad Naghshin
  # Web Site    : http://farzaad.ir
  # E-Mail      : naghshin.farzad@gmail.com
  # Co Web Site : https://satyarpg.com
  #

  # File        : user
  # Create      : Oct 12, 2018 4:34:57 PM
  # Encoding    : UTF-8
 */

class user extends Controller {

    private $refererUrl;

    function __construct() {
        parent::__construct();
        Session::init();
        $refererUrl = $_SERVER['HTTP_REFERER'];
        $refererUrl = explode('?', $refererUrl);
        $this->refererUrl = $refererUrl[0];
    }

    //
    //
    // public functions
    //
    //

    public function index()
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $this->view->userlist = $this->model->getList();
        $this->view->render('header');
        $this->view->render('user/list');
        $this->view->render('footer');
    }

    public function create()
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $this->view->render('header');
        $this->view->render('user/create');
        $this->view->render('footer');
    }

    public function edit($userid)
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $this->view->userInfo = $this->model->loadItem($userid);
        $this->view->render('header');
        $this->view->render('user/edit');
        $this->view->render('footer');
    }


    public function login()
    {
        $this->view->render('user/login');
    }



    public function profile()
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $this->view->userinfo = $this->model->loadProfile();
        $this->view->render('header');
        $this->view->render('user/profile');
        $this->view->render('footer');
    }

    //
    //
    // public with out user interface
    //
    //

    public function runlogin()
    {
        $mail = $_POST['mail'];
        $pass = $_POST['pass'];
        $result = $this->model->login($mail,$pass);
        if($result['error'])
        {
            header('Location:'.$this->refererUrl.Core::msg($result));
        }else{
            header('Location:'.adminurl.Core::msg($result));
        }
    }

    public function runforgetpass()
    {

    }

    public function editProfile($id)
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $userid = Session::get('userid');
        if($userid < 1 || $id != $userid)
            header('Location:'.adminurl);
        $fullname = $_POST['fullname'];
        $mobile = $_POST['mobile'];
        $phone = $_POST['phone'];
        $birthday = $_POST['birthday'];
        $gender = $_POST['gender'];
        $about = $_POST['about'];
        $result = $this->model->editprofile($fullname,$mobile,$phone,$birthday,$gender,$about);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }

    public function editAvatar($id)
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $userid = Session::get('userid');
        if($userid < 1 || $id != $userid)
            header('Location:'.adminurl);
        $avatar = $_FILES['avatar'];
        $result = $this->model->uploadAvatar($avatar);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }

    public function editPass($id)
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $userid = Session::get('userid');
        if($userid < 1 || $id != $userid)
            header('Location:'.adminurl);
        $oldpass = $_POST['oldpass'];
        $newpass = $_POST['newpass'];
        $renewpass = $_POST['renewpass'];
        $result = $this->model->changePass($oldpass,$newpass,$renewpass);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }

    public function runcreate()
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $fullname = $_POST['fullname'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $mail = $_POST['mail'];
        $avatar = $_FILES['avatar'];
        $bio = $_POST['bio'];
        $type = $_POST['type'];
        $result = $this->model->create($fullname,$username,$password,$mail,$avatar,$bio,$type);
        if($result['error'])
        {
            header('Location:'.$this->refererUrl.Core::msg($result));
        }else{
            header('Location:'.adminurl.'/user/edit/'.$result['info']['id'].Core::msg($result));
        }
    }

    public function runedit($id)
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $fullname = $_POST['fullname'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $mail = $_POST['mail'];
        $avatar = $_FILES['avatar'];
        $bio = $_POST['bio'];
        $type = $_POST['type'];
        $result = $this->model->edit($id,$fullname,$username,$password,$mail,$avatar,$bio,$type);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }

    public function active($userid)
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $result = $this->model->changeStatus($userid,1);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }

    public function deactive($userid)
    {
        Core::_tokenCheck();
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $result = $this->model->changeStatus($userid,0);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }

    public function logout()
    {
        @session_start();
        @session_destroy();
        header('Location:'.adminurl);
    }

    //
    //
    // public for ajax
    //
    //




    //
    //
    // private functions
    //
    //



}
