<?php

/*
  # Programmer  : Farzad Naghshin
  # Web Site    : http://farzaad.ir
  # E-Mail      : naghshin.farzad@gmail.com
  # Co Web Site : https://satyarpg.com
  #

  # File        : user
  # Create      : Oct 12, 2018 4:34:57 PM
  # Encoding    : UTF-8
 */

class cat extends Controller {

    private $refererUrl;

    function __construct() {
        parent::__construct();
        Session::init();
        $refererUrl = $_SERVER['HTTP_REFERER'];
        $refererUrl = explode('?', $refererUrl);
        $this->refererUrl = $refererUrl[0];
    }

    //
    //
    // public functions
    //
    //

    public function index()
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $this->view->catlist = $this->model->getList();
        $this->view->render('header');
        $this->view->render('category/list');
        $this->view->render('footer');
    }

    public function create()
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $this->view->render('header');
        $this->view->render('category/create');
        $this->view->render('footer');
    }

    public function edit($userid)
    {
        if(!Core::isAdmin())
        {
            header('Location:'.adminurl);
            die('Access Error !');
        }
        $this->view->catInfo = $this->model->loadItem($userid);
        $this->view->render('header');
        $this->view->render('category/edit');
        $this->view->render('footer');
    }

    public function runcreate()
    {
        $title = $_POST['title'];
        $result = $this->model->add($title);
        if($result['error'])
        {
            header('Location:'.$this->refererUrl.Core::msg($result));
        }else{
            header('Location:'.adminurl.'/cat/edit/'.$result['info']['id'].Core::msg($result));
        }
    }

    public function runedit($id)
    {
        $title = $_POST['title'];
        $result = $this->model->edit($id,$title);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }

    public function del($id)
    {
        $result = $this->model->del($id);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }

    //
    //
    // public for ajax
    //
    //




    //
    //
    // private functions
    //
    //



}
