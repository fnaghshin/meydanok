<?php


class post extends Controller {

    private $refererUrl;

    function __construct() {
        parent::__construct();
        Session::init();
        $this->LoadModel('core', mPath, 'core');
        $refererUrl = $_SERVER['HTTP_REFERER'];
        $refererUrl = explode('?', $refererUrl);
        $this->refererUrl = $refererUrl[0];
    }

    //
    //
    // public functions
    //
    //

    public function index()
    {
        $this->view->postList = $this->model->getList();
        $this->view->render('header');
        $this->view->render('post/list');
        $this->view->render('footer');
    }

    public function create()
    {
        $this->LoadModel('cat',mPath,'cat');
        $this->view->catList = $this->cat->getList();
        $this->view->render('header');
        $this->view->render('post/create');
        $this->view->render('footer');
    }

    public function edit($id)
    {
        $this->LoadModel('cat',mPath,'cat');
        $this->view->catList = $this->cat->getList();
        $this->view->postInfo = $this->model->loadItem($id);
        $this->view->render('header');
        $this->view->render('post/edit');
        $this->view->render('footer');
    }

    public function runcreate()
    {
        $title = $_POST['title'];
        $key = $_POST['key'];
        $cat = $_POST['cat'];
        $text = $_POST['text'];
        $image = $_FILES['image'];
        $result = $this->model->create($title,$key,$cat,$text,$image);
        if($result['error'])
        {
            header('Location:'.$this->refererUrl.Core::msg($result));
        }else{
            header('Location:'.adminurl.'post/edit/'.$result['info']['id'].Core::msg($result));
        }
    }

    public function runedit($id)
    {
        $title = $_POST['title'];
        $key = $_POST['key'];
        $cat = $_POST['cat'];
        $text = $_POST['text'];
        $image = $_FILES['image'];
        $result = $this->model->edit($id,$title,$key,$cat,$text,$image);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }

    public function del($id)
    {
        $result = $this->model->del($id);
        header('Location:'.$this->refererUrl.Core::msg($result));
    }





    //
    //
    //  Ajax Functions
    //
    //

    public function getPostKey()
    {
        $title = $_POST['title'];
        echo $this->model->getKey($title);
    }




    //
    //
    // private functions
//
    //



}
