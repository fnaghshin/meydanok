<?php

class Index extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        if(Core::isAdmin()) {
            header('Location:' . adminurl . 'dashboard');
        }else{
            header('Location:' . adminurl . 'user/login');
        }
    }

}

?>
