<?php

/*
  # Programmer  : Farzad Naghshin
  # Web Site    : http://farzaad.ir
  # E-Mail      : naghshin.farzad@gmail.com
  # Co Web Site : https://satyarpg.com
  #

  # File        : dashboard
  # Create      : Jun 11, 2018 7:15:08 PM
  # Encoding    : UTF-8
 */

class dashboard extends Controller {

    private $refererUrl;

    function __construct() {
        parent::__construct();
        Session::init();
        $this->LoadModel('core', mPath, 'core');
        $refererUrl = $_SERVER['HTTP_REFERER'];
        $refererUrl = explode('?', $refererUrl);
        $this->refererUrl = $refererUrl[0];
    }

    //
    //
    // public functions
    //
    //

    public function index()
    {
        $this->view->totalUser = $this->model->totalUser();
        $this->view->totalCat = $this->model->totalCat();
        $this->view->totalPost = $this->model->totalPost();
        $this->view->render('header');
        $this->view->render('dashboard/index');
        $this->view->render('footer');
    }







    //
    //
    // private functions
//
    //



}
