<?php


class core_model extends Model {

    private $_userid;

    public function __construct() {
        parent::__construct();
        $this->_userid = Session::get('userid');
    }

    //
    //
    // public functions
    //
    //

    public function catList()
    {
        $query = $this->db->prepare("SELECT * FROM `category`");
        $query->execute();
        return $query->fetchAll();
    }

    public function postlist()
    {
        $query = $this->db->prepare("SELECT * FROM `post` ORDER BY `created_at` DESC");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function postlistByCat($catid)
    {
        $query = $this->db->prepare("SELECT * FROM `post` WHERE `cat`=:catid ORDER BY `created_at` DESC");
        $query->execute(array(':catid'=>$catid));
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function loadPostByKey($key)
    {
        $query = $this->db->prepare("SELECT * FROM `post` WHERE `key`=:key");
        $query->execute(array(':key'=>$key));
        return $query->fetch(PDO::FETCH_ASSOC);
    }

}
